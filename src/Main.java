import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Main implements ActionListener{
    JFrame frame = new JFrame("Gui World Demo");
    JMenuBar menuBar = new JMenuBar();
    JMenu mnSearch,mnFormat,mnFile, mnIntranet, mnInternet;
    JMenuItem mniNetwork, mniLocal, mniOpen, mniExit;
    JButton btnStartDemo = new JButton("Start Demo");
    public Main(){
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,200);
        frame.setJMenuBar(menuBar);
        frame.setLayout(new BorderLayout());

        //Menus Instantiation
        mnSearch = new JMenu("Search");
        mnFormat = new JMenu("Format");
        mnFile = new JMenu("File");
        mnIntranet = new JMenu("Intranet");
        mnInternet = new JMenu("Internet");

        //Menu Items Instantiation
        mniNetwork = new JMenuItem("Network");
        mniLocal = new JMenuItem("Local");
        mniOpen = new JMenuItem("Open");
        mniExit = new JMenuItem("Exit");

        //Add Menus To The MenuBar
        menuBar.add(mnSearch);
        menuBar.add(mnFormat);
        menuBar.add(mnFile);

        //Add Menu Items to the Menues
        mnSearch.add(mniNetwork);
        mnSearch.add(mniLocal);
        mnFile.add(mniOpen);
        mnFile.add(mniExit);

        //Add SubMenus To the Menu Item
        mniNetwork.add(mnIntranet);
        mniNetwork.add(mnInternet);

        //Add other Components To The Frame
        frame.add(btnStartDemo, BorderLayout.CENTER);

        //AddActionListeners to MenuItems
        mniExit.addActionListener(this);
        btnStartDemo.addActionListener(this);

        //Make Frame Visible
        frame.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==mniExit){
            int choice = JOptionPane.showConfirmDialog(null,"Do you Want To Exit The Program?");
            if(choice==0){
                JOptionPane.showMessageDialog(null,"Program Terminated");
                System.exit(0);
            }
        }else{
            String name = JOptionPane.showInputDialog("Enter Your Name: ");
            int age = Integer.parseInt(JOptionPane.showInputDialog("Enter Your Age: "));
            if(age<18)
                JOptionPane.showMessageDialog(null, name+" You Are Still A Child");
            else
                JOptionPane.showMessageDialog(null, name+" You Are An Adult!!. Watch Porn\uD83D\uDE02\uD83D\uDE01");
        }
    }
    public static void main(String[] vick){
        new Main();
    }
}
